use attheme::Attheme;
use attheme_editor_api::{download, upload, Error, Theme, ThemeId};

const NAME: &str = "Night Light";
const THEME: &[u8] = b"windowBackgroundWhite=#ff000000";

#[tokio::test]
async fn it_works() -> Result<(), Error> {
    let theme = Attheme::from_bytes(THEME);
    let ThemeId { theme_id } = upload(NAME, &theme).await?;
    let Theme { name, content } = download(&theme_id).await?;

    assert_eq!(name, NAME);
    assert_eq!(content, theme);

    Ok(())
}
