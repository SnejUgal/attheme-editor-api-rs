use attheme::{Attheme, ColorSignature};
use reqwest::Client;
use serde::{
    de::{self, DeserializeOwned, Deserializer, Visitor},
    Deserialize, Serialize,
};
use std::fmt::{self, Formatter};

const API_URL: &str = "https://attheme-editor.snejugal.ru/api/";

#[derive(Debug, PartialEq, Eq, Clone, Hash, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct ThemeId {
    pub theme_id: String,
}

#[derive(Debug, PartialEq, Clone, Deserialize)]
pub struct Theme {
    pub name: String,
    #[serde(deserialize_with = "attheme_from_base64")]
    pub content: Attheme,
}

struct AtthemeVisitor;

impl<'v> Visitor<'v> for AtthemeVisitor {
    type Value = Attheme;

    fn expecting(&self, formatter: &mut Formatter) -> fmt::Result {
        write!(formatter, "an Attheme serialized as a Base64 string")
    }

    fn visit_str<E: de::Error>(self, string: &str) -> Result<Attheme, E> {
        let bytes = base64::decode(string)
            .map_err(|x| de::Error::custom(x.to_string()))?;
        Ok(Attheme::from_bytes(&bytes))
    }
}

fn attheme_from_base64<'de, D>(deserializer: D) -> Result<Attheme, D::Error>
where
    D: Deserializer<'de>,
{
    deserializer.deserialize_str(AtthemeVisitor)
}

#[derive(Deserialize)]
struct RawError {
    error: String,
}

#[derive(Debug)]
pub enum Error {
    BadRequest(String),
    Reqwest(reqwest::Error),
}

impl From<reqwest::Error> for Error {
    fn from(error: reqwest::Error) -> Self {
        Self::Reqwest(error)
    }
}

#[derive(Serialize, Clone)]
#[serde(rename_all = "camelCase", tag = "requestType")]
enum Request<'a> {
    UploadTheme {
        name: &'a str,
        content: String,
    },
    #[serde(rename_all = "camelCase")]
    DownloadTheme {
        theme_id: &'a str,
    },
}

#[derive(Deserialize)]
#[serde(untagged)]
enum Response<T> {
    Error(RawError),
    Ok(T),
}

impl<T> Into<Result<T, Error>> for Response<T> {
    fn into(self) -> Result<T, Error> {
        match self {
            Response::Ok(ok) => Ok(ok),
            Response::Error(RawError { error }) => {
                Err(Error::BadRequest(error))
            }
        }
    }
}

async fn request<T: DeserializeOwned>(
    request: Request<'_>,
) -> Result<T, Error> {
    let client = Client::new();
    let post = client.post(API_URL).json(&request);
    let request = post.build()?;
    let response = client.execute(request).await?;
    let raw_response: Response<T> = response.json().await?;

    raw_response.into()
}

pub async fn upload(name: &str, theme: &Attheme) -> Result<ThemeId, Error> {
    let serialized = theme.to_bytes(ColorSignature::Hex);
    let content = base64::encode(&serialized);
    request(Request::UploadTheme { name, content }).await
}

pub async fn download(theme_id: &str) -> Result<Theme, Error> {
    request(Request::DownloadTheme { theme_id }).await
}
